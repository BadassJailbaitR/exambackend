package emi.ac.ma.exam.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public InMemoryUserDetailsManager UserDetailsService() {
        UserDetails voteur = User.withDefaultPasswordEncoder().username("voteur").password("root").roles("VOTEUR").build();
        UserDetails organisateur = User.withDefaultPasswordEncoder().username("organisateur").password("root").roles("ORGANISATEUR").build();
        return new InMemoryUserDetailsManager(voteur, organisateur);
    }

    @Bean
    public SecurityFilterChain config(HttpSecurity http) throws Exception {
        return http.csrf(csrf -> csrf.disable()).authorizeRequests(auth -> {
            auth.requestMatchers("/api/bulletin/**").permitAll();
            auth.requestMatchers("/api/Personne/**").hasAnyRole("ORGANISATEUR","VOTEUR");
            auth.requestMatchers("/api/plage-horaire/**").hasRole("ORGANISATEUR");
            auth.requestMatchers("/api/preference/**").hasRole("ORGANISATEUR");
            auth.requestMatchers("/api/scrulin-plage-horaire/**").hasRole("ORGANISATEUR");
            auth.requestMatchers("/api/scrulin-preference/**").hasRole("ORGANISATEUR");
        }).build();
    }
}