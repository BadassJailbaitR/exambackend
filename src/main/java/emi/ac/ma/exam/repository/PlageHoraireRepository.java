package emi.ac.ma.exam.repository;

import emi.ac.ma.exam.entity.PlageHoraire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlageHoraireRepository extends JpaRepository<PlageHoraire,Long> {
    Optional<PlageHoraire> findPlageHoraireById(Long id);
}
