package emi.ac.ma.exam.repository;

import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ScrulinPlagesHorairesRepository extends JpaRepository<ScrulinPlagesHoraires, Long> {
    Optional<ScrulinPlagesHoraires> findScrulinPlagesHorairesById(Long id);

}
