package emi.ac.ma.exam.repository;

import emi.ac.ma.exam.entity.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonneRepository extends JpaRepository<Personne,Long> {
    Optional<Personne> findPersonneById(Long id);
}
