package emi.ac.ma.exam.repository;

import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import emi.ac.ma.exam.entity.ScrulinPreferences;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ScrulinPreferencesRepository extends JpaRepository<ScrulinPreferences,Long> {
    Optional<ScrulinPreferences> findScrulinPreferencesById(Long id);
}
