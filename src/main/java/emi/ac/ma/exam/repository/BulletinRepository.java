package emi.ac.ma.exam.repository;

import emi.ac.ma.exam.entity.Bulletin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BulletinRepository extends JpaRepository<Bulletin,Long> {
    Optional<Bulletin> findBulletinById(Long id);
    Optional<Bulletin> findAllByPourOuContre(boolean pourOuContre);
}
