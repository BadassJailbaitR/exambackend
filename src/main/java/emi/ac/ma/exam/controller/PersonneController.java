package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.models.response.PersonneResponse;
import emi.ac.ma.exam.service.PersonneService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/personne")
public class PersonneController {
    private final PersonneService personneService;

    public PersonneController(PersonneService personneService) {
        this.personneService = personneService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonneResponse> getPersonne(@PathVariable Long id) {
        return ResponseEntity.ok(personneService.getPersonne(id));
    }

    @PostMapping("/add")
    public ResponseEntity<PersonneResponse> getPersonne(@RequestBody Personne personne) {
        return new ResponseEntity<>(personneService.savePersonne(personne), HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<PersonneResponse>> getAllPersonne() {
        return ResponseEntity.ok(personneService.getAllPersonne());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePersonne(@PathVariable Long id) {
        personneService.deletePersonne(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Personne> updatePersonne(@RequestBody Personne personne,@PathVariable Long id) {
        return ResponseEntity.ok(personneService.updatePersonne(personne,id));
    }


}
