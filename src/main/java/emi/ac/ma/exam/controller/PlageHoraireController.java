package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.PlageHoraire;
import emi.ac.ma.exam.models.response.PlageHoraireResponse;
import emi.ac.ma.exam.service.PlageHoraireService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/plage-horaire")
public class PlageHoraireController {
    private final PlageHoraireService plageHoraireService;

    public PlageHoraireController(PlageHoraireService plageHoraireService) {
        this.plageHoraireService = plageHoraireService;
    }

    @GetMapping("/all")
    public List<PlageHoraire> getAllPlageHoraire() {
        return plageHoraireService.getAllPlageHoraire();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlageHoraire> getPlageHoraire(@PathVariable Long id) {
        return ResponseEntity.ok(plageHoraireService.getPlageHoraire(id));
    }

    @PostMapping("/add/{idScrulin}")
    public ResponseEntity<PlageHoraire> savePlageHoraire(@RequestBody PlageHoraire plageHoraire,@PathVariable Long idScrulin) {
        return new ResponseEntity<>(plageHoraireService.savePlageHoraire(plageHoraire,idScrulin), HttpStatus.CREATED);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<PlageHoraire> updatePlageHoraire(@RequestBody PlageHoraire plageHoraire,@PathVariable Long id) {
        return ResponseEntity.ok(plageHoraireService.updatePlageHoraire(plageHoraire,id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePlageHoraire(@PathVariable Long id) {
        plageHoraireService.deletePlageHoraire(id);
        return ResponseEntity.noContent().build();
    }



}
