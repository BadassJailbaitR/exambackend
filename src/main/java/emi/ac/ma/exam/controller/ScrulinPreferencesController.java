package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.ScrulinPreferences;
import emi.ac.ma.exam.service.ScrulinPreferencesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/scrulin-preference")
public class ScrulinPreferencesController {
    private final ScrulinPreferencesService scrulinPreferencesService;

    public ScrulinPreferencesController(ScrulinPreferencesService scrulinPreferencesService) {
        this.scrulinPreferencesService = scrulinPreferencesService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ScrulinPreferences> getScrulinPreferences(@PathVariable Long id) {
        return ResponseEntity.ok(scrulinPreferencesService.getScrulinPreferences(id));
    }

    @PostMapping("/add/{idPersonne}")
    public ResponseEntity<ScrulinPreferences> saveScrulinPreferences(@RequestBody ScrulinPreferences scrulinPreferences,@PathVariable Long idPersonne) {
        return new ResponseEntity<>(scrulinPreferencesService.saveScrulinPreferences(scrulinPreferences,idPersonne), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ScrulinPreferences> updateScrulinPreferences(@RequestBody ScrulinPreferences scrulinPreferences,@PathVariable Long id) {
        return ResponseEntity.ok(scrulinPreferencesService.updateScrulinPreferences(scrulinPreferences,id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteScrulinPreferences(@PathVariable Long id) {
        scrulinPreferencesService.deleteScrulinPreferences(id);
        return ResponseEntity.noContent().build();
    }




}
