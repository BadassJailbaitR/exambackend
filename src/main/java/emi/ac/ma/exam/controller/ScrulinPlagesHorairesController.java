package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import emi.ac.ma.exam.service.ScrulinPlagesHorairesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/scrulin-plage-horaire")
public class ScrulinPlagesHorairesController {
    private final ScrulinPlagesHorairesService scrulinPlagesHorairesService;

    public ScrulinPlagesHorairesController(ScrulinPlagesHorairesService scrulinPlagesHorairesService) {
        this.scrulinPlagesHorairesService = scrulinPlagesHorairesService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ScrulinPlagesHoraires> getScrulinPlagesHoraires(@PathVariable Long id) {
        return ResponseEntity.ok(scrulinPlagesHorairesService.getScrulinPlagesHoraires(id));
    }

    @PostMapping("/add/{idPersonne}")
    public ResponseEntity<ScrulinPlagesHoraires> saveScrulinPlagesHoraires(@RequestBody ScrulinPlagesHoraires scrulinPlagesHoraires,@PathVariable Long idPersonne) {
        return new ResponseEntity<>(scrulinPlagesHorairesService.saveScrulinPlagesHoraires(scrulinPlagesHoraires,idPersonne), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ScrulinPlagesHoraires> updateScrulinPlagesHoraires(@RequestBody ScrulinPlagesHoraires scrulinPlagesHoraires,@PathVariable Long id) {
        return ResponseEntity.ok(scrulinPlagesHorairesService.updateScrulinPlagesHoraires(scrulinPlagesHoraires,id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteScrulinPlagesHoraires( @PathVariable Long id) {
        scrulinPlagesHorairesService.deleteScrulinPlagesHoraires(id);
        return ResponseEntity.noContent().build();
    }


}
