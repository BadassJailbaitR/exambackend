package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.Bulletin;
import emi.ac.ma.exam.models.request.BulletinRequest;
import emi.ac.ma.exam.models.response.BulletinResponse;
import emi.ac.ma.exam.service.BulletinService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bulletin")
public class BulletinController {
    private final BulletinService bulletinService;

    public BulletinController(BulletinService bulletinService) {
        this.bulletinService = bulletinService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BulletinResponse> getBulletin(@PathVariable Long id) {
        return ResponseEntity.ok(bulletinService.getBulletin(id));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Bulletin>> getBulletin() {
        return ResponseEntity.ok(bulletinService.getAllBulletin());
    }

    @PostMapping("/add/{idPersonne}")
    public ResponseEntity<BulletinResponse> getBulletin(@RequestBody BulletinRequest request, @PathVariable Long idPersonne) {
        return new ResponseEntity<>(bulletinService.saveBulletin(request,idPersonne), HttpStatus.CREATED);
    }

    @GetMapping("/all/pour")
    public ResponseEntity<List<Bulletin>> getBulletinPour() {
        return ResponseEntity.ok(bulletinService.getAllBulletinPour());
    }

    @GetMapping("/all/contre")
    public ResponseEntity<List<Bulletin>> getBulletinContre() {
        return ResponseEntity.ok(bulletinService.getAllBulletinContre());
    }

    //Il n y aura pas de put dans bulletin car le bulletin une fois donner pas de changement
    //Aussi pour le delete une fois tu submit le bulletin vous n a pas le droit de la supprimer


}
