package emi.ac.ma.exam.controller;

import emi.ac.ma.exam.entity.Preference;
import emi.ac.ma.exam.service.PreferenceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/preference")
public class PreferenceController {
    private final PreferenceService preferenceService;

    public PreferenceController(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Preference> getPreference(@PathVariable Long id) {
        return ResponseEntity.ok(preferenceService.getPreference(id));
    }

    @PostMapping("/add/{idScrulin}")
    public ResponseEntity<Preference> savePreference(@RequestBody Preference preference,@PathVariable Long idScrulin) {
        return ResponseEntity.ok(preferenceService.savePreference(preference,idScrulin));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Preference> updatePreference(@RequestBody Preference preference,@PathVariable Long id) {
        return ResponseEntity.ok(preferenceService.updatePreference(preference,id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePreference(@PathVariable Long id) {
        preferenceService.deletePreference(id);
        return ResponseEntity.noContent().build();
    }




}
