package emi.ac.ma.exam.models.response;

import emi.ac.ma.exam.entity.Bulletin;

import java.util.List;

public class PersonneResponse {
    private String nom;
    private String prenom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
