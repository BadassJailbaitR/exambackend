package emi.ac.ma.exam.models.response;

public class BulletinResponse {
    private boolean pourOuContre;

    public boolean isPourOuContre() {
        return pourOuContre;
    }

    public void setPourOuContre(boolean pourOuContre) {
        this.pourOuContre = pourOuContre;
    }
}
