package emi.ac.ma.exam.models.request;

public class BulletinRequest {
    private boolean pourOuContre;

    public boolean isPourOuContre() {
        return pourOuContre;
    }

    public void setPourOuContre(boolean pourOuContre) {
        this.pourOuContre = pourOuContre;
    }
}
