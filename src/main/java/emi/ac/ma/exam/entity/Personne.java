package emi.ac.ma.exam.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long nbTotalParticipations;
    private Long nbTotalOrganisations;
    private Long nbParticipations;
    private Long nbOrganisations;
    private String nom;
    private String prenom;

    @OneToMany
    @JoinTable(name = "bulletinsPersonne")
    private List<Bulletin> bulletins = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNbTotalParticipations() {
        return nbTotalParticipations;
    }

    public void setNbTotalParticipations(Long nbTotalParticipations) {
        this.nbTotalParticipations = nbTotalParticipations;
    }

    public Long getNbTotalOrganisations() {
        return nbTotalOrganisations;
    }

    public void setNbTotalOrganisations(Long nbTotalOrganisations) {
        this.nbTotalOrganisations = nbTotalOrganisations;
    }

    public Long getNbParticipations() {
        return nbParticipations;
    }

    public void setNbParticipations(Long nbParticipations) {
        this.nbParticipations = nbParticipations;
    }

    public Long getNbOrganisations() {
        return nbOrganisations;
    }

    public void setNbOrganisations(Long nbOrganisations) {
        this.nbOrganisations = nbOrganisations;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<Bulletin> getBulletins() {
        return bulletins;
    }

    public void setBulletins(List<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nbTotalParticipations=" + nbTotalParticipations +
                ", nbTotalOrganisations=" + nbTotalOrganisations +
                ", nbParticipations=" + nbParticipations +
                ", nbOrganisations=" + nbOrganisations +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", bulletins=" + bulletins +
                '}';
    }
}
