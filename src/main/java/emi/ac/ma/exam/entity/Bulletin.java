package emi.ac.ma.exam.entity;

import jakarta.persistence.*;

@Entity
public class Bulletin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "TINYINT")
    private boolean pourOuContre;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPourOuContre() {
        return pourOuContre;
    }

    public void setPourOuContre(boolean pourOuContre) {
        this.pourOuContre = pourOuContre;
    }



    @Override
    public String toString() {
        return "Bulletin{" +
                "id=" + id +
                ", pourOuContre=" + pourOuContre +
                '}';
    }
}
