package emi.ac.ma.exam.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

@Entity
public class ScrulinPreferences extends Scrulin {
    private Long nbTotal;
    @OneToMany(mappedBy = "preference")
    private List<Preference> preferences = new ArrayList<>();

    public Long getNbTotal() {
        return nbTotal;
    }

    public void setNbTotal(Long nbTotal) {
        this.nbTotal = nbTotal;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }

    @Override
    public String toString() {
        return "ScrulinPreferences{" +
                "nbTotal=" + nbTotal +
                ", preferences=" + preferences +
                '}';
    }
}
