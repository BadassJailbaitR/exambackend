package emi.ac.ma.exam.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

@Entity
public class ScrulinPlagesHoraires extends Scrulin{
    private Long nbTotal;
    @OneToMany(mappedBy = "scrulunHoraires")
    private List<PlageHoraire> plageHoraires = new ArrayList<>();

    public List<PlageHoraire> getPlageHoraires() {
        return plageHoraires;
    }

    public void setPlageHoraires(List<PlageHoraire> plageHoraires) {
        this.plageHoraires = plageHoraires;
    }

    public Long getNbTotal() {
        return nbTotal;
    }

    public void setNbTotal(Long nbTotal) {
        this.nbTotal = nbTotal;
    }



    @Override
    public String toString() {
        return "ScrulinPlagesHoraires{" +
                "nbTotal=" + nbTotal +
                '}';
    }
}
