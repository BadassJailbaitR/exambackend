package emi.ac.ma.exam.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import java.util.Date;

@Entity
public class PlageHoraire extends Choix {
    private Date date;
    private Date heureDebut;
    private Date heureFin;
    @ManyToOne
    @JoinColumn(name = "scrulinHoraires_id")
    private ScrulinPlagesHoraires scrulunHoraires;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Date heureDebut) {
        this.heureDebut = heureDebut;
    }

    public Date getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Date heureFin) {
        this.heureFin = heureFin;
    }

    public ScrulinPlagesHoraires getScrulunHoraires() {
        return scrulunHoraires;
    }

    public void setScrulunHoraires(ScrulinPlagesHoraires scrulunHoraires) {
        this.scrulunHoraires = scrulunHoraires;
    }

    @Override
    public String toString() {
        return "PlageHoraire{" +
                "date=" + date +
                ", heureDebut=" + heureDebut +
                ", heureFin=" + heureFin +
                ", scrulunHoraires=" + scrulunHoraires +
                '}';
    }
}
