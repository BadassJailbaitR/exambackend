package emi.ac.ma.exam.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Choix {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    protected Long nbBulletinsPour;
    protected Long nbBulletinsContre;
    @OneToMany
    @JoinTable(name = "bulletinsChoix")
    protected List<Bulletin> bulletins = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNbBulletinsPour() {
        return nbBulletinsPour;
    }

    public void setNbBulletinsPour(Long nbBulletinsPour) {
        this.nbBulletinsPour = nbBulletinsPour;
    }

    public Long getNbBulletinsContre() {
        return nbBulletinsContre;
    }

    public void setNbBulletinsContre(Long nbBulletinsContre) {
        this.nbBulletinsContre = nbBulletinsContre;
    }

    public List<Bulletin> getBulletins() {
        return bulletins;
    }

    public void setBulletins(List<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    @Override
    public String toString() {
        return "Choix{" +
                "id=" + id +
                ", nbBulletinsPour=" + nbBulletinsPour +
                ", nbBulletinsContre=" + nbBulletinsContre +
                ", bulletins=" + bulletins +
                '}';
    }
}
