package emi.ac.ma.exam.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Scrulin {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;
    protected String intituleScrulin;
    protected String textCommentaireScrulin;
    protected Date dateCreation;
    protected Date dateDebut;
    protected Date dateLimite;
    protected Date dateLimiteExistence;
    protected boolean ouvertureAvancee;
    protected boolean fermetureAvancee;
    protected boolean destructionAvancee;
    protected Long nbTotalScrulin;
    protected Long nbChoix;
    @ManyToOne
    @JoinColumn(name = "organisateur_id")
    protected Personne organisateur;
    @OneToMany
    @JoinTable(name = "bulletinsScrulin")
    protected List<Bulletin> bulletins = new ArrayList<>();

    public String getIntituleScrulin() {
        return intituleScrulin;
    }

    public void setIntituleScrulin(String intituleScrulin) {
        this.intituleScrulin = intituleScrulin;
    }

    public String getTextCommentaireScrulin() {
        return textCommentaireScrulin;
    }

    public void setTextCommentaireScrulin(String textCommentaireScrulin) {
        this.textCommentaireScrulin = textCommentaireScrulin;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(Date dateLimite) {
        this.dateLimite = dateLimite;
    }

    public Date getDateLimiteExistence() {
        return dateLimiteExistence;
    }

    public void setDateLimiteExistence(Date dateLimiteExistence) {
        this.dateLimiteExistence = dateLimiteExistence;
    }

    public boolean isOuvertureAvancee() {
        return ouvertureAvancee;
    }

    public void setOuvertureAvancee(boolean ouvertureAvancee) {
        this.ouvertureAvancee = ouvertureAvancee;
    }

    public boolean isFermetureAvancee() {
        return fermetureAvancee;
    }

    public void setFermetureAvancee(boolean fermetureAvancee) {
        this.fermetureAvancee = fermetureAvancee;
    }

    public boolean isDestructionAvancee() {
        return destructionAvancee;
    }

    public void setDestructionAvancee(boolean destructionAvancee) {
        this.destructionAvancee = destructionAvancee;
    }

    public Long getNbTotalScrulin() {
        return nbTotalScrulin;
    }

    public void setNbTotalScrulin(Long nbTotalScrulin) {
        this.nbTotalScrulin = nbTotalScrulin;
    }

    public Personne getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(Personne organisateur) {
        this.organisateur = organisateur;
    }

    public List<Bulletin> getBulletins() {
        return bulletins;
    }

    public void setBulletins(List<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNbChoix() {
        return nbChoix;
    }

    public void setNbChoix(Long nbChoix) {
        this.nbChoix = nbChoix;
    }

    @Override
    public String toString() {
        return "Scrulin{" +
                "intituleScrulin='" + intituleScrulin + '\'' +
                ", textCommentaireScrulin='" + textCommentaireScrulin + '\'' +
                ", dateCreation=" + dateCreation +
                ", dateDebut=" + dateDebut +
                ", dateLimite=" + dateLimite +
                ", dateLimiteExistence=" + dateLimiteExistence +
                ", ouvertureAvancee=" + ouvertureAvancee +
                ", fermetureAvancee=" + fermetureAvancee +
                ", destructionAvancee=" + destructionAvancee +
                ", nbTotalScrulin=" + nbTotalScrulin +
                ", organisateur=" + organisateur +
                ", bulletins=" + bulletins +
                '}';
    }

}
