package emi.ac.ma.exam.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Preference extends Choix {
    private String intitulePreference;
    private String texteCommentairePref;
    @ManyToOne
    @JoinColumn(name = "preference_id")
    private ScrulinPreferences preference;

    public String getIntitulePreference() {
        return intitulePreference;
    }

    public void setIntitulePreference(String intitulePreference) {
        this.intitulePreference = intitulePreference;
    }

    public String getTexteCommentairePref() {
        return texteCommentairePref;
    }

    public void setTexteCommentairePref(String texteCommentairePref) {
        this.texteCommentairePref = texteCommentairePref;
    }

    public ScrulinPreferences getPreference() {
        return preference;
    }

    public void setPreference(ScrulinPreferences preference) {
        this.preference = preference;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "intitulePreference='" + intitulePreference + '\'' +
                ", texteCommentairePref='" + texteCommentairePref + '\'' +
                ", preference=" + preference +
                '}';
    }
}
