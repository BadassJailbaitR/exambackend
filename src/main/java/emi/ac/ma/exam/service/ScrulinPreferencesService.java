package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.ScrulinPreferences;
import org.springframework.stereotype.Service;

@Service
public interface ScrulinPreferencesService {
    ScrulinPreferences getScrulinPreferences(Long id);
    ScrulinPreferences saveScrulinPreferences(ScrulinPreferences scrulinPreferences,Long idPersonne);
    ScrulinPreferences updateScrulinPreferences(ScrulinPreferences scrulinPreferences,Long id);
    void deleteScrulinPreferences(Long id);

}
