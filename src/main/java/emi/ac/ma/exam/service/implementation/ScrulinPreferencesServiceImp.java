package emi.ac.ma.exam.service.implementation;


import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.entity.ScrulinPreferences;
import emi.ac.ma.exam.repository.PersonneRepository;
import emi.ac.ma.exam.repository.ScrulinPreferencesRepository;
import emi.ac.ma.exam.service.ScrulinPreferencesService;
import org.springframework.stereotype.Service;

@Service
public class ScrulinPreferencesServiceImp implements ScrulinPreferencesService {
    private final ScrulinPreferencesRepository scrulinPreferencesRepository;
    private final PersonneRepository personneRepository;

    public ScrulinPreferencesServiceImp(ScrulinPreferencesRepository scrulinPreferencesRepository, PersonneRepository personneRepository) {
        this.scrulinPreferencesRepository = scrulinPreferencesRepository;
        this.personneRepository = personneRepository;
    }

    @Override
    public void deleteScrulinPreferences(Long id) {
        scrulinPreferencesRepository.deleteById(id);
    }

    @Override
    public ScrulinPreferences getScrulinPreferences(Long id) {
        return scrulinPreferencesRepository.findScrulinPreferencesById(id).orElse(null);
    }

    @Override
    public ScrulinPreferences saveScrulinPreferences(ScrulinPreferences scrulinPreferences,Long idPersonne) {
        Personne personne = personneRepository.findPersonneById(idPersonne).orElse(null);
        scrulinPreferences.setOrganisateur(personne);
        personne.setNbOrganisations(personne.getNbOrganisations()+1);
        return scrulinPreferencesRepository.save(scrulinPreferences);
    }

    @Override
    public ScrulinPreferences updateScrulinPreferences(ScrulinPreferences scrulinPreferences,Long id) {
        ScrulinPreferences updatedScrulinPreferences = scrulinPreferencesRepository.findScrulinPreferencesById(id).orElse(null);
        updatedScrulinPreferences.setPreferences(scrulinPreferences.getPreferences());
        updatedScrulinPreferences.setNbTotal(scrulinPreferences.getNbTotal());
        return scrulinPreferencesRepository.save(scrulinPreferences);
    }




}
