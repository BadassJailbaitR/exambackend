package emi.ac.ma.exam.service.implementation;


import emi.ac.ma.exam.entity.Preference;
import emi.ac.ma.exam.entity.ScrulinPreferences;
import emi.ac.ma.exam.repository.PreferenceRepository;
import emi.ac.ma.exam.repository.ScrulinPreferencesRepository;
import emi.ac.ma.exam.service.PreferenceService;
import org.springframework.stereotype.Service;

@Service
public class PreferenceServiceImp implements PreferenceService {
    private final PreferenceRepository preferenceRepository;
    private final ScrulinPreferencesRepository scrulinPreferencesRepository;

    public PreferenceServiceImp(PreferenceRepository preferenceRepository, ScrulinPreferencesRepository scrulinPreferencesRepository) {
        this.preferenceRepository = preferenceRepository;
        this.scrulinPreferencesRepository = scrulinPreferencesRepository;
    }

    @Override
    public void deletePreference(Long id) {
        preferenceRepository.deleteById(id);
    }

    @Override
    public Preference getPreference(Long id) {
        return preferenceRepository.findPreferenceById(id).orElse(null);
    }

    @Override
    public Preference savePreference(Preference preference,Long idScrulin) {
        ScrulinPreferences scrulinPreferences = scrulinPreferencesRepository.findScrulinPreferencesById(idScrulin).orElse(null);
        preference.setPreference(scrulinPreferences);
        scrulinPreferences.getPreferences().add(preference);
        scrulinPreferences.setNbChoix(scrulinPreferences.getNbChoix()+1);
        scrulinPreferencesRepository.save(scrulinPreferences);
        return preferenceRepository.save(preference);
    }

    @Override
    public Preference updatePreference(Preference preference,Long id) {
        Preference updatedPreference = preferenceRepository.findPreferenceById(id).orElse(null);
        updatedPreference.setIntitulePreference(preference.getIntitulePreference());
        updatedPreference.setTexteCommentairePref(preference.getTexteCommentairePref());
        return preferenceRepository.save(preference);
    }


}
