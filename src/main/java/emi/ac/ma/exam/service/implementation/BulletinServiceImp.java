package emi.ac.ma.exam.service.implementation;

import emi.ac.ma.exam.entity.Bulletin;
import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.models.request.BulletinRequest;
import emi.ac.ma.exam.models.response.BulletinResponse;
import emi.ac.ma.exam.repository.BulletinRepository;
import emi.ac.ma.exam.repository.PersonneRepository;
import emi.ac.ma.exam.service.BulletinService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BulletinServiceImp implements BulletinService {
    private final BulletinRepository bulletinRepository;
    private  final PersonneRepository personneRepository;

    public BulletinServiceImp(BulletinRepository bulletinRepository, PersonneRepository personneRepository) {
        this.bulletinRepository = bulletinRepository;
        this.personneRepository = personneRepository;
    }

    @Override
    public BulletinResponse getBulletin(Long id) {
        Bulletin bulletin = bulletinRepository.findBulletinById(id).orElse(null);
        if(bulletin==null) {
            return null;
        }
        BulletinResponse response = new BulletinResponse();
        response.setPourOuContre(bulletin.isPourOuContre());
        return response;
    }

    @Override
    public BulletinResponse saveBulletin(BulletinRequest request, Long idPersonne) {
        Bulletin bulletin = new Bulletin();
        bulletin.setPourOuContre(request.isPourOuContre());
        bulletinRepository.save(bulletin);
        BulletinResponse response = new BulletinResponse();
        response.setPourOuContre(bulletin.isPourOuContre());
        Personne personne = personneRepository.findPersonneById(idPersonne).orElse(null);
        personne.getBulletins().add(bulletin);
        personne.setNbParticipations(personne.getNbParticipations()+1);
        personneRepository.save(personne);
        return response;
    }

    @Override
    public List<Bulletin> getAllBulletin() {
        List<Bulletin> bulletins = bulletinRepository.findAll();
        return bulletins;
    }

    @Override
    public List<Bulletin> getAllBulletinPour() {
        List<Bulletin> bulletins = (List<Bulletin>) bulletinRepository.findAllByPourOuContre(true).orElse(null);
        return bulletins;
    }

    @Override
    public List<Bulletin> getAllBulletinContre() {
        List<Bulletin> bulletins = (List<Bulletin>) bulletinRepository.findAllByPourOuContre(false).orElse(null);
        return bulletins;
    }


}
