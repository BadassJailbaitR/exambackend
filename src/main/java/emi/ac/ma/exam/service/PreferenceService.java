package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.Preference;
import org.springframework.stereotype.Service;

@Service
public interface PreferenceService {
    void deletePreference(Long id);
    Preference getPreference(Long id);
    Preference savePreference(Preference preference,Long idScrulin);
    Preference updatePreference(Preference preference,Long id);

}
