package emi.ac.ma.exam.service.implementation;

import emi.ac.ma.exam.entity.PlageHoraire;
import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import emi.ac.ma.exam.models.response.PlageHoraireResponse;
import emi.ac.ma.exam.repository.PlageHoraireRepository;
import emi.ac.ma.exam.repository.ScrulinPlagesHorairesRepository;
import emi.ac.ma.exam.service.PlageHoraireService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlageHoraireServiceImp implements PlageHoraireService {
    private final PlageHoraireRepository plageHoraireRepository;
    private final ScrulinPlagesHorairesRepository scrulinPlagesHorairesRepository;

    public PlageHoraireServiceImp(PlageHoraireRepository plageHoraireRepository, ScrulinPlagesHorairesRepository scrulinPlagesHorairesRepository) {
        this.plageHoraireRepository = plageHoraireRepository;
        this.scrulinPlagesHorairesRepository = scrulinPlagesHorairesRepository;
    }

    @Override
    public List<PlageHoraire> getAllPlageHoraire() {
        return plageHoraireRepository.findAll();
    }

    @Override
    public PlageHoraire getPlageHoraire(Long id) {
        return plageHoraireRepository.findPlageHoraireById(id).orElse(null);
    }

    @Override
    public PlageHoraire savePlageHoraire(PlageHoraire plageHoraire,Long idScrulin) {
        ScrulinPlagesHoraires scrulinPlagesHoraires = scrulinPlagesHorairesRepository.findScrulinPlagesHorairesById(idScrulin).orElse(null);
        plageHoraire.setScrulunHoraires(scrulinPlagesHoraires);
        scrulinPlagesHoraires.getPlageHoraires().add(plageHoraire);
        scrulinPlagesHoraires.setNbChoix(scrulinPlagesHoraires.getNbChoix()+1);
        scrulinPlagesHorairesRepository.save(scrulinPlagesHoraires);
        return plageHoraireRepository.save(plageHoraire);
    }

    @Override
    public PlageHoraire updatePlageHoraire(PlageHoraire plageHoraire,Long id) {
        PlageHoraire updatedPlageHoraire = plageHoraireRepository.findPlageHoraireById(id).orElse(null);
        updatedPlageHoraire.setHeureDebut(plageHoraire.getHeureDebut());
        updatedPlageHoraire.setHeureFin(plageHoraire.getHeureFin());
        updatedPlageHoraire.setDate(plageHoraire.getDate());
        return plageHoraireRepository.save(plageHoraire);
    }

    @Override
    public void deletePlageHoraire(Long id) {
        plageHoraireRepository.deleteById(id);
    }




}
