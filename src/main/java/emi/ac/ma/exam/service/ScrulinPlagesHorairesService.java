package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import org.springframework.stereotype.Service;

@Service
public interface ScrulinPlagesHorairesService {
    void deleteScrulinPlagesHoraires(Long id);
    ScrulinPlagesHoraires getScrulinPlagesHoraires(Long id);
    ScrulinPlagesHoraires saveScrulinPlagesHoraires(ScrulinPlagesHoraires scrulinPlagesHoraires,Long idPersonne);
    ScrulinPlagesHoraires updateScrulinPlagesHoraires(ScrulinPlagesHoraires scrulinPlagesHoraires,Long id);
}
