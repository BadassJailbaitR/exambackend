package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.PlageHoraire;
import emi.ac.ma.exam.models.response.PlageHoraireResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PlageHoraireService {
    List<PlageHoraire> getAllPlageHoraire();
    PlageHoraire getPlageHoraire(Long id);
    PlageHoraire savePlageHoraire(PlageHoraire plageHoraire, Long idPlageHoraire);
    PlageHoraire updatePlageHoraire(PlageHoraire plageHoraire,  Long id);
    void deletePlageHoraire(Long id);
}
