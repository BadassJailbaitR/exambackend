package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.Bulletin;
import emi.ac.ma.exam.models.request.BulletinRequest;
import emi.ac.ma.exam.models.response.BulletinResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BulletinService {
    BulletinResponse getBulletin(Long id);
    BulletinResponse saveBulletin(BulletinRequest request, Long idPersonne);
    List<Bulletin> getAllBulletin();
    List<Bulletin> getAllBulletinPour();
    List<Bulletin> getAllBulletinContre();
}
