package emi.ac.ma.exam.service.implementation;

import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.models.response.PersonneResponse;
import emi.ac.ma.exam.repository.PersonneRepository;
import emi.ac.ma.exam.service.PersonneService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonneServiceImp implements PersonneService {
    private final PersonneRepository personneRepository;

    public PersonneServiceImp(PersonneRepository personneRepository) {
        this.personneRepository = personneRepository;
    }

    @Override
    public PersonneResponse getPersonne(Long id) {
        //On utilise PersonneResponse pour ne pas afficher les donne sensible du personne donc juste le nom
        //et le prenom
        Personne personne = personneRepository.findPersonneById(id).orElse(null);
        PersonneResponse response = new PersonneResponse();
        response.setNom(personne.getNom());
        response.setPrenom(personne.getPrenom());
        return response;
    }

    @Override
    public PersonneResponse savePersonne(Personne personne) {
        //On utilise PersonneResponse pour ne pas afficher les donne sensible du personne donc juste le nom
        //et le prenom
        personneRepository.save(personne);
        PersonneResponse response = new PersonneResponse();
        response.setNom(personne.getNom());
        response.setPrenom(personne.getPrenom());
        return response;
    }

    @Override
    public List<PersonneResponse> getAllPersonne() {
        List<Personne> personnes = personneRepository.findAll();

        //On utilise PersonneResponse pour ne pas afficher les donne sensible du personne donc juste le nom
        //et le prenom
        List<PersonneResponse> personneResponses = new ArrayList<>();
        for (Personne personne : personnes) {
            PersonneResponse personneResponse = new PersonneResponse();
            personneResponse.setNom(personne.getNom());
            personneResponse.setPrenom(personne.getPrenom());
            personneResponses.add(personneResponse);
        }
        return personneResponses;
    }

    @Override
    public void deletePersonne(Long id) {
        personneRepository.deleteById(id);
    }

    @Override
    public Personne updatePersonne(Personne personne,Long id) {
        Personne updatedPersonne = personneRepository.findPersonneById(id).orElse(null);
        updatedPersonne.setNom(personne.getNom());
        updatedPersonne.setPrenom(personne.getPrenom());
        updatedPersonne.setBulletins(personne.getBulletins());
        updatedPersonne.setNbOrganisations(personne.getNbOrganisations());
        updatedPersonne.setNbParticipations(personne.getNbParticipations());
        updatedPersonne.setNbTotalOrganisations(personne.getNbTotalOrganisations());
        updatedPersonne.setNbTotalParticipations(personne.getNbTotalParticipations());
        return personneRepository.save(personne);
    }
}
