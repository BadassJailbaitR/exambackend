package emi.ac.ma.exam.service.implementation;

import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.entity.ScrulinPlagesHoraires;
import emi.ac.ma.exam.repository.PersonneRepository;
import emi.ac.ma.exam.repository.ScrulinPlagesHorairesRepository;
import emi.ac.ma.exam.service.ScrulinPlagesHorairesService;
import org.springframework.stereotype.Service;

@Service
public class ScrulinPlagesHorairesServiceImp implements ScrulinPlagesHorairesService {
    private final ScrulinPlagesHorairesRepository scrulinPlagesHorairesRepository;
    private final PersonneRepository personneRepository;

    public ScrulinPlagesHorairesServiceImp(ScrulinPlagesHorairesRepository scrulinPlagesHorairesRepository, PersonneRepository personneRepository) {
        this.scrulinPlagesHorairesRepository = scrulinPlagesHorairesRepository;
        this.personneRepository = personneRepository;
    }

    @Override
    public void deleteScrulinPlagesHoraires(Long id) {
        scrulinPlagesHorairesRepository.deleteById(id);
    }

    @Override
    public ScrulinPlagesHoraires getScrulinPlagesHoraires(Long id) {
        return scrulinPlagesHorairesRepository.findScrulinPlagesHorairesById(id).orElse(null);
    }

    @Override
    public ScrulinPlagesHoraires saveScrulinPlagesHoraires(ScrulinPlagesHoraires scrulinPlagesHoraires,Long idPersonne) {
        Personne personne = personneRepository.findPersonneById(idPersonne).orElse(null);
        scrulinPlagesHoraires.setOrganisateur(personne);
        personne.setNbOrganisations(personne.getNbOrganisations()+1);
        System.out.println(scrulinPlagesHoraires);
        return scrulinPlagesHorairesRepository.save(scrulinPlagesHoraires);
    }

    @Override
    public ScrulinPlagesHoraires updateScrulinPlagesHoraires(ScrulinPlagesHoraires scrulinPlagesHoraires,Long id) {
        ScrulinPlagesHoraires updatedScrulinPlagesHoraires = scrulinPlagesHorairesRepository.findScrulinPlagesHorairesById(id).orElse(null);
        updatedScrulinPlagesHoraires.setPlageHoraires(scrulinPlagesHoraires.getPlageHoraires());
        updatedScrulinPlagesHoraires.setNbTotal(scrulinPlagesHoraires.getNbTotal());
        return scrulinPlagesHorairesRepository.save(scrulinPlagesHoraires);
    }


}
