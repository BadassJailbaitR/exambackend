package emi.ac.ma.exam.service;

import emi.ac.ma.exam.entity.Personne;
import emi.ac.ma.exam.models.response.PersonneResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonneService {
    PersonneResponse getPersonne(Long id);
    PersonneResponse savePersonne(Personne personne);
    Personne updatePersonne(Personne personne,Long id);
    List<PersonneResponse> getAllPersonne();
    void deletePersonne(Long id);

}
